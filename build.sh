#!/usr/bin/bash
#
# vAOSP Build Script
# Copyright (C) 2022 Vaisakh Murali
#

set -e

while getopts b: flag; do
  case "${flag}" in
    b) BUILD_BRANCH=${OPTARG} ;;
  esac
done

# We will be using minimal alpine docker env for building.
export WORKDIR=$PWD
echo "Setup workspace"

# Setup repo workspace
mkdir $WORKDIR/aosp/
cd $WORKDIR/aosp

# Init the source manifest (init with --depth=1 to sync shallow)
repo init -u https://android.googlesource.com/platform/manifest -b $BUILD_BRANCH --depth=1

# Sync up local manifest
mkdir .repo/local_manifests/
curl -o .repo/local_manifests/cannon.xml https://gitlab.com/aosp-on-cannon/manifest/-/raw/android-12/cannon.xml

# Start the main sync
repo sync --no-tags --no-clone-bundle -j$(nproc --all)

# Clear project .git to save space
rm -rf .repo

# Start build process
cd $WORKDIR/aosp/
source build/envsetup.sh

# Setup the build target
lunch cannon-user

# Build the final otapackage
m otapackage -j$(nproc --all)
